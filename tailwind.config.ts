import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at a50% 50%, var(--tw-gradient-stops))',
      },
      minHeight: {
        content: "calc(100vh - 56px)",
      },
      colors: {
        primary: "#345FFF",
        success: "#07A128",
        error: "#FF3B38",
        warning: "#122F41",
        description: "#718098",
        "branch-1": "#345FFF",
        "text-4": "#082555",
        gray: {
          0: "#FFFFFF",
          100: "#34404B",
          200: "#082555",
          300: "#F3F5F9",
          400: "#75818E",
        },
        white: {
          0: "#ffffff",
          100: "#C9D4E4",
          200: "#ECEEF4",
          300: "#C9D4E4",
          400: "#D6DCE2",
        },
      },
      spacing: {},
      borderRadius: {
        primary: "10px",
      },
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
    },
    fontSize: {
      /** Heading */
      h0: ["6rem", "7rem"],
      h1: ["3.5rem", "4.5rem"],
      h2: ["3rem", "3.5rem"],
      h3: ["2.5rem", "3rem"],
      h4: ["2rem", "2.5rem"],
      h5: ["1.5rem", "2rem"],

      "h-d": ["6rem", "7rem"],
      "h-xl": ["3.5rem", "4.5rem"],
      "h-lg": ["3rem", "3.5rem"],
      "h-md": ["2.5rem", "3rem"],
      "h-sm": ["2rem", "2.5rem"],
      "h-xs": ["1.5rem", "2rem"],
      "h-xxs": ["1.25rem", "1.5rem"],

      /** Subtitle */
      "s-xl": ["3rem", "3.5rem"],
      "s-l": ["2.5rem", "3rem"],
      "s-md": ["2rem", "2.5rem"],
      "s-sm": ["1.5rem", "2rem"],

      /** body */
      xs: ["0.75rem", "1rem"],
      sm: ["0.875rem", "1.25rem"],
      base: ["1rem", "1.5rem"],
      md: ["1.125rem", "1.75rem"],
      lg: ["1.25rem", "1.75rem"],
    },
    fontFamily: {
      inter: ["Inter", "sans-serif"],
    },
    boxShadow: {
      header: "0px 1px 40px 0px rgba(152, 159, 186, 0.10)",
      popup: "0px 1px 15px 1px rgba(9, 14, 24, 0.10)",
    },
  },
  plugins: [
    require("tailwindcss")("./tailwindcss-config.js"),
    "prettier-plugin-tailwindcss",
  ],
  corePlugins: {
    preflight: false,
  }
}
export default config

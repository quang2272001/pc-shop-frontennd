import { createProduct, getDetailProduct, getListCatalog, updateProduct } from '@/service/api.service';
import { CreateProductPayload } from '@/types/payload.type';
import { Catalog } from '@/types/response.type';
import { ModalProps } from '@/utils/interface';
import { showCustomNotification } from '@/utils/notification';
import { PlusOutlined, UploadOutlined } from '@ant-design/icons';
import {
    Button,
    Col,
    Input,
    InputNumber,
    Modal,
    Row,
    Select,
    Upload,
} from 'antd';
import { UploadProps } from 'antd/lib';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';

interface CreateProductModalProps extends ModalProps {
    productId: string;
}

const CreateProductModal = ({ handleCancle, handleOk, isModalOpen, productId }: CreateProductModalProps) => {
    const initCreateProductPayload = {
        name: '',
        description: '',
        images: [],
        price: 1,
        properties: '',
        quantity: 1,
        catalogIds: [],
    };
    const [createProductPayload, setCreateProductPayload] =
        useState<CreateProductPayload>(initCreateProductPayload);
    const [listCatalog, setListCatalog] = useState<Catalog[]>([]);

    useEffect(() => {
        const getData = async () => {
            try {
                const responseCatalog = await getListCatalog({ page: 1, limit: 9999 });
                setListCatalog(responseCatalog.data);
                if (productId) {
                    const response = await getDetailProduct(productId as string);
                    setCreateProductPayload(response.data);
                }
            } catch (error) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: 'Đã có lỗi xảy ra!',
                });
            }
        };
        getData();
    }, [productId]);

    useEffect(() => {
        const getData = async () => {
            try {
                const responseCatalog = await getListCatalog({ page: 1, limit: 9999 });
                setListCatalog(responseCatalog.data);
            } catch (error) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: 'Đã có lỗi xảy ra!',
                });
            }
        };
        getData();
    }, []);

    const props: UploadProps = {
        name: 'file',
        multiple: true,
        onChange(info) {
            setCreateProductPayload({
                ...createProductPayload,
                images: info.fileList.map(file => file.originFileObj as File),
            });
        },
    };

    const handleSave = async () => {
        try {
            if (!createProductPayload.name) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: 'Tên không được trống!',
                });
                return;
            }
            if (!createProductPayload.description) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: 'Mô tả không được trống!',
                });
                return;
            }
            if (createProductPayload.catalogIds.length < 1) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: 'Loại sản phẩm không được trống!',
                });
                return;
            }
            if (!createProductPayload.properties) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: 'Tên không được trống!',
                });
                return;
            }
            if (!productId) {
                await createProduct(createProductPayload)
            } else {
                const { images, ...updateProductPayload } = createProductPayload;
                await updateProduct(productId, updateProductPayload);
            }
            handleOk();
        } catch (error) {
            showCustomNotification({
                status: 'error',
                message: 'Lỗi',
                description: 'Đã có lỗi xảy ra!',
            });
        }
    };

    const footerModal = (
        <div className="flex items-center justify-center gap-4 my-8">
            <Button
                onClick={() => { handleCancle() }}
                type="text"
                className="bg-white-200 hover:opacity-70 w-36"
                size="middle"
            >
                Hủy
            </Button>
            <Button
                onClick={() => {
                    handleSave();
                }}
                type="primary"
                size="middle"
                className="w-36"
            >
                {productId ? 'Sửa' : 'Tạo'}
            </Button>
        </div>
    );

    return (
        <>
            <Modal
                title={<h3 className="text-center text-h5 ">{productId ? 'Sửa' : 'Thêm'} sản phẩm</h3>}
                width={800}
                className="p-4"
                footer={footerModal}
                onCancel={handleCancle}
                open={isModalOpen}
            >
                <Row className="mt-8" gutter={4}>
                    <Col span={6}>Tên sản phẩm</Col>
                    <Col span={18}>
                        <Input
                            value={createProductPayload.name}
                            onChange={e => {
                                setCreateProductPayload({
                                    ...createProductPayload,
                                    name: e.target.value,
                                });
                            }}
                        />
                    </Col>
                </Row>
                <Row className="mt-8" gutter={4}>
                    <Col span={6}>Loại sản phẩm</Col>
                    <Col span={18}>
                        <Select
                            className="w-full"
                            value={createProductPayload.catalogIds}
                            options={listCatalog.map(v => {
                                return {
                                    label: v.name,
                                    value: v.id,
                                };
                            })}
                            onChange={value => {
                                setCreateProductPayload({
                                    ...createProductPayload,
                                    catalogIds: value,
                                });
                            }}
                            mode="multiple"
                        />
                    </Col>
                </Row>
                <Row className="mt-8" gutter={4}>
                    <Col span={6}>Mô tả</Col>
                    <Col span={18}>
                        <TextArea
                            value={createProductPayload.description}
                            onChange={e => {
                                setCreateProductPayload({
                                    ...createProductPayload,
                                    description: e.target.value,
                                });
                            }}
                        />
                    </Col>
                </Row>
                <Row className="mt-8" gutter={4}>
                    <Col span={6}>Thuộc tính</Col>
                    <Col span={18}>
                        <TextArea
                            value={createProductPayload.properties}
                            onChange={e => {
                                setCreateProductPayload({
                                    ...createProductPayload,
                                    properties: e.target.value,
                                });
                            }}
                            placeholder="tên thuộc tính1: thuộc tính1 |tên thuộc tính2: thuộc tính2 |"
                        />
                    </Col>
                </Row>
                <Row className="mt-8" gutter={4}>
                    <Col span={6}>Giá</Col>
                    <Col span={18}>
                        <InputNumber
                            min={1}
                            max={10000000000}
                            className='w-full'
                            value={createProductPayload.price}
                            onChange={e => {
                                setCreateProductPayload({
                                    ...createProductPayload,
                                    price: Number(e),
                                });
                            }}
                        />
                    </Col>
                </Row>
                <Row className="mt-8" gutter={4}>
                    <Col span={6}>Số lượng</Col>
                    <Col span={18}>
                        <InputNumber
                            value={createProductPayload.quantity}
                            className='w-full'
                            onChange={e => {
                                setCreateProductPayload({
                                    ...createProductPayload,
                                    quantity: Number(e),
                                });
                            }}
                            min={1}
                            max={10000000000}
                        />
                    </Col>
                </Row>
                {!productId && <Row className="mt-8" gutter={4}>
                    <Col span={6}>Ảnh</Col>
                    <Col span={18}>
                        <Upload {...props}>
                            <Button icon={<UploadOutlined />}>
                                Select File
                            </Button>
                        </Upload>
                    </Col>
                </Row>}
            </Modal>
        </>
    );
};

export default CreateProductModal;

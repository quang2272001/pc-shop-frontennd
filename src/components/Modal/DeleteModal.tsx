import { ModalProps } from '@/utils/interface'
import { DeleteOutlined } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import React from 'react'

interface DeleteModalProps extends ModalProps {
    title: string;
    productId: string;
}
const DeleteModal = ({ handleCancle, isModalOpen, title, handleOk }: DeleteModalProps) => {
    const footerModal = (
        <div className="flex items-center justify-center gap-4 pt-4 p-2">
            <Button
                onClick={handleOk}
                type="primary"
                size="middle"
                className="min-w-[126px]"
            >
                Có
            </Button>
            <Button
                type="text"
                className="bg-white-200 min-w-[126px] hover:opacity-70"
                size="middle"
                onClick={handleCancle}
            >
                Không
            </Button>
        </div>
    );
    return (
        <>
            <Modal
                open={isModalOpen}
                onCancel={handleCancle}
                footer={footerModal}
                className="max-w-[350px]"
                title={<div className=' text-center text-error' > <DeleteOutlined /></div>}
            >
                <div className="flex flex-col items-center mt-6 gap-4">
                    <p className="text-base font-semibold text-center">{title}</p>
                </div>
            </Modal>
        </>
    );
}

export default DeleteModal
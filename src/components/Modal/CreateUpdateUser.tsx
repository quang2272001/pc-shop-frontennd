import { createUser, getUserDetail, updateUser } from '@/service/api.service';
import { User } from '@/types/response.type';
import { ROLE } from '@/utils/constance';
import { handleException } from '@/utils/functions';
import { ModalProps } from '@/utils/interface';
import { showCustomNotification } from '@/utils/notification';
import { Button, Input, Modal, Row, Select } from 'antd';
import { AxiosError } from 'axios';
import React, { useEffect, useState } from 'react';

interface CreateUpdateUserProps extends ModalProps {
    userId: string;
}

const CreateUpdateUser = ({
    handleCancle,
    handleOk,
    isModalOpen,
    userId,
}: CreateUpdateUserProps) => {
    const initUser = {
        address: '',
        email: '',
        fullname: '',
        phoneNumber: '',
        role: ROLE.USER,
        username: '',
        password: '',
    };
    const [userInfo, setUserInfo] = useState<User>(initUser);

    const submit = async () => {
        try {
            userId
                ? await updateUser(userId, userInfo)
                : await createUser({
                      ...userInfo,
                      password: String(userInfo.password),
                  });
            userId
                ? showCustomNotification({
                      status: 'success',
                      message: 'Thành công',
                      description: 'Thêm user thành công',
                  })
                : showCustomNotification({
                      status: 'success',
                      message: 'Thành công',
                      description: 'Sửa user thành công',
                  });
            handleOk();
            setUserInfo(initUser);
        } catch (error: any) {
            handleException(error);
        }
    };

    useEffect(() => {
        const getData = async () => {
            const response = await getUserDetail(userId);
            setUserInfo(response.data);
        };
        if (userId) {
            getData();
        }
    }, [userId]);

    const footerModal = (
        <div className="flex items-center justify-center gap-4 my-8">
            <Button
                onClick={() => {
                    handleCancle();
                }}
                type="text"
                className="bg-white-200 hover:opacity-70 w-36"
                size="middle"
            >
                Hủy
            </Button>
            <Button
                onClick={() => {
                    submit();
                }}
                type="primary"
                size="middle"
                className="w-36"
            >
                {userId ? 'Sửa' : 'Tạo'}
            </Button>
        </div>
    );

    return (
        <Modal
            footer={footerModal}
            title={
                <h3 className="text-center text-h5">
                    {userId ? 'Sửa User' : 'Thêm User'}
                </h3>
            }
            onCancel={handleCancle}
            open={isModalOpen}
        >
            <div>
                <Row className="mt-6">
                    <label className="mb-2">Email *</label>
                    <Input
                        size="large"
                        type="email"
                        value={userInfo.email}
                        onChange={e => {
                            setUserInfo({ ...userInfo, email: e.target.value });
                        }}
                        placeholder="Email"
                    />
                </Row>
                {!userId && (
                    <Row className="mt-4">
                        <label className="mb-2">Password *</label>
                        <Input
                            size="large"
                            type="password"
                            placeholder="Password"
                            value={userInfo.password}
                            onChange={e => {
                                setUserInfo({
                                    ...userInfo,
                                    password: e.target.value,
                                });
                            }}
                        />
                    </Row>
                )}
                <Row className="mt-4">
                    <label className="mb-2">UserName *</label>
                    <Input
                        size="large"
                        placeholder="UserName"
                        value={userInfo.username}
                        onChange={e => {
                            setUserInfo({
                                ...userInfo,
                                username: e.target.value,
                            });
                        }}
                    />
                </Row>
                <Row className="mt-4">
                    <label className="mb-2">Họ tên *</label>
                    <Input
                        size="large"
                        placeholder="Họ tên"
                        value={userInfo.fullname}
                        onChange={e => {
                            setUserInfo({
                                ...userInfo,
                                fullname: e.target.value,
                            });
                        }}
                    />
                </Row>
                <Row className="mt-4">
                    <label className="mb-2">Địa chỉ *</label>
                    <Input
                        size="large"
                        placeholder="Địa chỉ"
                        value={userInfo.address}
                        onChange={e => {
                            setUserInfo({
                                ...userInfo,
                                address: e.target.value,
                            });
                        }}
                    />
                </Row>
                <Row className="mt-4">
                    <label className="mb-2">Số điện thoại *</label>
                    <Input
                        size="large"
                        placeholder="Địa chỉ"
                        value={userInfo.phoneNumber}
                        onChange={e => {
                            setUserInfo({
                                ...userInfo,
                                phoneNumber: e.target.value,
                            });
                        }}
                    />
                </Row>
                <Row className="mt-4">
                    <label className="mb-2">Chức vụ *</label>
                    <Select
                        className="w-full"
                        value={userInfo.role}
                        onSelect={e => {
                            setUserInfo({ ...userInfo, role: e });
                        }}
                        options={[
                            {
                                label: 'ADMIN',
                                value: ROLE.ADMIN,
                            },
                            {
                                label: 'USER',
                                value: ROLE.USER,
                            },
                        ]}
                    />
                </Row>
            </div>
        </Modal>
    );
};

export default CreateUpdateUser;

import React from "react";
import { Spin } from "antd";

const LoadingComponent = () => (
    <div className="h-screen grid place-items-center">
        <Spin size="large" />
    </div>
);
export default LoadingComponent;


import { getOrderDetail, updateOrder } from '@/service/api.service';
import { Order, Product } from '@/types/response.type';
import { ORDER_STATUS, RECEIVE_TYPE, ROLE } from '@/utils/constance';
import { converMoney, totalMoney } from '@/utils/functions';
import { showCustomNotification } from '@/utils/notification';
import { Button, Col, Input, Row, Select } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { Table } from 'antd/lib';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

export const OrderDetailComponent = ({ role }: { role: ROLE }) => {
    const router = useRouter();
    const { orderId } = router.query;
    const [orderDetail, setOrderDetail] = useState<Order>({
        id: 0,
        addressReceiver: '',
        listProduct: [],
        nameReceiver: '',
        phoneReceiver: '',
        status: ORDER_STATUS.all,
        type: RECEIVE_TYPE.RECEIVE_AT_HOME,
    });
    useEffect(() => {
        const getData = async () => {
            try {
                if (orderId) {
                    const response = await getOrderDetail(orderId as string);
                    setOrderDetail(response.data);
                }
            } catch (error) { }
        };
        getData();
    }, [orderId]);

    const handleUpdate = async () => {
        try {
            console.log(orderDetail);
            const { id, listProduct, ...updateOrderPayload } = orderDetail;
            await updateOrder(String(id), updateOrderPayload);
            showCustomNotification({
                status: 'success',
                message: 'Thành công',
                description: 'Cập nhật đơn hàng thành công',
            });
        } catch (error) {
            showCustomNotification({
                status: 'error',
                message: 'Lỗi',
                description: 'Đã có lỗi xảy ra!',
            });
        }
    }

    function convertStatus(status: ORDER_STATUS) {
        switch (status) {
            case ORDER_STATUS.created:
                return <div className="text-sm">Chờ vận chuyển</div>;
            case ORDER_STATUS.shipped:
                return <div className="text-sm">Đang vận chuyển</div>;
            case ORDER_STATUS.received:
                return <div className="text-sm">Đã nhận</div>;

            default:
                return <div className="text-sm">Đã huỷ</div>;
        }
    }

    const columns: ColumnsType<{
        product: Product;
        quantity: number;
    }> = [
            {
                title: 'Ảnh',
                key: 'image',
                render: (value, record) => (
                    <div>
                        <img
                            src={record.product.images[0]}
                            width={120}
                            height={120}
                        />
                    </div>
                ),
                width: '20%',
            },
            {
                title: 'Tên',
                dataIndex: 'name',
                key: 'name',
                render: (text, record) => (
                    <div className="text-sm">{record.product.name}</div>
                ),
                width: '20%',
            },
            {
                title: 'Giá',
                dataIndex: 'price',
                key: 'price',
                width: '20%',
                render: (price, record) => (
                    <div className="text-base text-gray-400">
                        {converMoney(record.product.price)}
                    </div>
                ),
            },
            {
                title: 'Số lượng',
                width: '10%',
                dataIndex: 'quantity',
                key: 'quantity',
            },
        ];

    return (
        <div>
            <Row className="mt-10">
                <Col span={4}>Mã đơn hàng: </Col>
                <Col span={10}>{orderDetail.id}</Col>
            </Row>
            <Row className="mt-5">
                <Col span={4}>Tên người nhận: </Col>
                <Col span={10}>
                    {role == ROLE.USER ? (
                        orderDetail.nameReceiver
                    ) : (
                        <Input
                            value={orderDetail.nameReceiver}
                            onChange={e => {
                                setOrderDetail({
                                    ...orderDetail,
                                    nameReceiver: e.target.value,
                                });
                            }}
                            className="w-1/2"
                        />
                    )}
                </Col>
            </Row>
            <Row className="mt-5">
                <Col span={4}>Địa chỉ: </Col>
                <Col span={10}>
                    {role == ROLE.USER ? (
                        orderDetail.addressReceiver
                    ) : (
                        <Input
                            value={orderDetail.addressReceiver}
                            onChange={e => {
                                setOrderDetail({
                                    ...orderDetail,
                                    addressReceiver: e.target.value,
                                });
                            }}
                            className="w-1/2"
                        />
                    )}
                </Col>
            </Row>
            <Row className="mt-5">
                <Col span={4}>Số điện thoại: </Col>
                <Col span={10}>
                    {role == ROLE.USER ? (
                        orderDetail.phoneReceiver
                    ) : (
                        <Input
                            value={orderDetail.phoneReceiver}
                            onChange={e => {
                                setOrderDetail({
                                    ...orderDetail,
                                    phoneReceiver: e.target.value,
                                });
                            }}
                            className="w-1/2"
                        />
                    )}
                </Col>
            </Row>
            <Row className="mt-5">
                <Col span={4}>Trạng thái: </Col>
                <Col span={10}>
                    {role == ROLE.USER ? (
                        convertStatus(orderDetail.status)
                    ) : (
                        <Select className='w-full'
                            value={orderDetail.status}
                            options={[
                                {
                                    label: convertStatus(ORDER_STATUS.created),
                                    value: ORDER_STATUS.created,
                                },
                                {
                                    label: convertStatus(ORDER_STATUS.shipped),
                                    value: ORDER_STATUS.shipped,
                                },
                                {
                                    label: convertStatus(ORDER_STATUS.received),
                                    value: ORDER_STATUS.received,
                                },
                                {
                                    label: convertStatus(ORDER_STATUS.cancled),
                                    value: ORDER_STATUS.cancled,
                                },
                            ]}
                            onSelect={(v) => {
                                setOrderDetail({ ...orderDetail, status: v })
                            }}
                        />
                    )}
                </Col>
            </Row>
            <Row className="mt-5">
                <Col span={4}>Hình thức nhận: </Col>
                <Col span={10}>
                    {role === ROLE.USER ? orderDetail.type == RECEIVE_TYPE.RECEIVE_AT_HOME
                        ? 'Nhận tại nhà'
                        : 'Nhận tại cửa hàng' : <Select className='w-full'
                            value={orderDetail.type}
                            options={[
                                {
                                    label: 'Nhận tại nhà',
                                    value: RECEIVE_TYPE.RECEIVE_AT_HOME,
                                },
                                {
                                    label: 'Nhận tại cửa hàng',
                                    value: RECEIVE_TYPE.RECEIVE_AT_STORE,
                                },
                            ]}
                            onSelect={(v) => {
                                setOrderDetail({ ...orderDetail, type: v })
                            }}
                    />}
                </Col>
            </Row>

            <Table
                className="mt-10"
                dataSource={orderDetail.listProduct}
                columns={columns}
                onRow={(record, rowIndex) => {
                    return {
                        onClick: (event) => {
                            router.push('/product/' + record.product.id)
                        },
                    }
                }}
            />

            <div className="flex justify-between items-center mt-4">
                <div className="text-base mt-5">
                    Tổng tiền: {totalMoney(orderDetail)}
                </div>
                <div>
                    <Button onClick={() => {
                        if (role === ROLE.ADMIN) router.push('/admin/orders')
                        else router.push("/")
                    }} >Thoát</Button>

                    {role === ROLE.ADMIN && <Button className='ml-4' type='primary' onClick={handleUpdate} >
                        Sửa
                    </Button>}
                </div>
            </div>
        </div>
    );
};


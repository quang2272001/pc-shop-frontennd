/* eslint-disable @next/next/no-img-element */
import { getCart, updateCart } from '@/service/api.service';
import { AddtoCartPayload, OrderPayload } from '@/types/payload.type';
import { Cart } from '@/types/response.type';
import { converMoney } from '@/utils/functions';
import { showCustomNotification } from '@/utils/notification';
import { DeleteOutlined } from '@ant-design/icons';
import { Button, InputNumber, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import Link from 'next/link';
import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
interface CartData extends Cart {
    key: number;
}

const TableProduct = ({
    setOrderPayload,
    setOrderTotal,
    deleteProduct,
    setDeleteProduct,
}: {
    setOrderPayload: Dispatch<SetStateAction<OrderPayload>>;
    setOrderTotal: Dispatch<SetStateAction<number>>;
    deleteProduct: AddtoCartPayload[];
    setDeleteProduct: Dispatch<SetStateAction<AddtoCartPayload[]>>;
}) => {
    const [cartData, setCartData] = useState<CartData[]>([]);
    const [reload, setReload] = useState(false);

    useEffect(() => {
        if (deleteProduct.length > 0) {
            const productIds = deleteProduct.map(v => v.id);
            const updateData = cartData.filter(
                v => !productIds.includes(v.key)
            );
            updateCart({
                cart: updateData.map(v => {
                    return { id: v.product.id, quantity: v.quantity };
                }),
            }).then(() => {
                setReload(!reload);
                setDeleteProduct([]);
            });
        }
    }, [deleteProduct]);

    useEffect(() => {
        getCart().then(response => {
            setCartData(
                response.data.map(v => {
                    return {
                        ...v,
                        key: v.product.id,
                    };
                })
            );
        });
    }, [reload]);

    const handleUpdateCart = async () => {
        try {
            await updateCart({
                cart: cartData.map(v => {
                    return { id: v.product.id, quantity: v.quantity };
                }),
            });
            setReload(!reload);
            showCustomNotification({
                status: 'success',
                message: 'Thành công',
                description: 'Giỏ hàng đã được cập nhật!',
            });
        } catch (error) {
            showCustomNotification({
                status: 'error',
                message: 'Lỗi',
                description: 'Đã có lỗi xảy ra!',
            });
        }
    };

    const columns: ColumnsType<CartData> = [
        {
            title: 'Sản Phẩm',
            render: (_, record) => (
                <Link href={'/product/' + record.product.id}>
                    <div className="flex gap-2">
                        <img
                            width={80}
                            height={80}
                            src={record.product.images[0]}
                            alt=""
                        />
                        <div style={{ fontSize: 14 }}>
                            {record.product.name}
                        </div>
                    </div>
                </Link>
            ),
            width: '45%',
        },
        {
            title: 'Đơn Giá',
            render: (_, record) => (
                <div className="text-gray-600 font-medium text-sm">
                    {converMoney(record.product.price)}
                </div>
            ),
        },
        {
            title: 'Số Lượng',
            render: (_, record) => (
                <div className="text-gray-600 font-medium text-sm">
                    <InputNumber
                        min={1}
                        max={100000}
                        value={record.quantity}
                        onChange={e => {
                            const index = cartData.findIndex(
                                v => v.key === record.key
                            );
                            cartData[index].quantity = Number(e);
                            setCartData([...cartData]);
                        }}
                    />
                </div>
            ),
        },
        {
            title: 'Số Tiền',
            render: (_, record) => (
                <div className="text-gray-600 font-medium text-sm">
                    {converMoney(record.product.price * record.quantity)}
                </div>
            ),
        },
        {
            title: '',
            render: (_, record) => (
                <div
                    className=" text-red-500 cursor-pointer "
                    onClick={() => {
                        setCartData([
                            ...cartData.filter(v => v.key !== record.key),
                        ]);
                    }}
                >
                    {' '}
                    <DeleteOutlined />
                </div>
            ),
        },
    ];

    // rowSelection object indicates the need for row selection
    const rowSelection = {
        onChange: (selectedRowKeys: React.Key[], selectedRows: CartData[]) => {
            let sum = 0;
            for (const data of selectedRows) {
                sum += data.product.price * data.quantity;
            }
            setOrderTotal(sum);
            setOrderPayload((pre: OrderPayload) => {
                return {
                    ...pre,
                    productInfos: selectedRows.map(v => {
                        return {
                            quantity: v.quantity,
                            id: v.product.id,
                        };
                    }),
                };
            });
        },
    };
    return (
        <>
            <Table
                rowSelection={{
                    ...rowSelection,
                }}
                pagination={false}
                columns={columns}
                dataSource={cartData}
            />
            <div className="mt-10 flex justify-center">
                <Button
                    type="primary"
                    className="rounded-lg"
                    onClick={() => {
                        handleUpdateCart();
                    }}
                >
                    Cập nhật giỏ hàng
                </Button>
            </div>
        </>
    );
};

export default TableProduct;

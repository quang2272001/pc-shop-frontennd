import { getListOrder, getListOrderAdmin } from '@/service/api.service';
import { Order } from '@/types/response.type';
import { ORDER_STATUS, RECEIVE_TYPE } from '@/utils/constance';
import { converMoney, totalMoney } from '@/utils/functions';
import { Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

const TableOrder = ({
    role,
    status,
}: {
    role: 'admin' | 'user';
    status: ORDER_STATUS;
}) => {
    const [listOrder, setListOrder] = useState<Order[]>([]);
    const router = useRouter();
    const columns: ColumnsType<Order> = [
        {
            title: 'id',
            dataIndex: 'id',
            key: 'id',
            width: '5%',
        },
        {
            title: 'Sản phẩm',
            key: 'product',
            render: (_, record) => (
                <>
                    <div className="flex">
                        <img
                            src={record.listProduct[0].product.images[0]}
                            width={120}
                            height={120}
                        />
                        <div>
                            <div className="text-base truncate w-64">
                                {record.listProduct[0].product.name}
                            </div>
                            <div className="text-sm">
                                {converMoney(
                                    record.listProduct[0].product.price
                                )}
                            </div>
                        </div>
                    </div>
                    <div>...</div>
                </>
            ),
            width: '40%',
        },
        {
            title: 'Tổng tiền',
            render: (_, record) => (
                <div className="text-sm">{totalMoney(record)}</div>
            ),
            width: '10%',
        },
        {
            title: 'Thông tin vận chuyển',
            render: (_, record) => (
                <div>
                    <div className="text-sm">
                        Tên người nhận: {record.nameReceiver}
                    </div>
                    <div className="text-sm">
                        Địa chỉ: {record.addressReceiver}{' '}
                    </div>
                    <div className="text-sm">
                        Số điện thoại: {record.phoneReceiver}{' '}
                    </div>
                </div>
            ),
        },
        {
            title: 'Trạng thái',
            render: (_, record) => {
                switch (record.status) {
                    case ORDER_STATUS.created:
                        return <div className="text-sm">Chờ vận chuyển</div>;
                    case ORDER_STATUS.shipped:
                        return <div className="text-sm">Đang vận chuyển</div>;
                    case ORDER_STATUS.received:
                        return <div className="text-sm">Đã nhận</div>;
                    default:
                        return <div className="text-sm">Đã huỷ</div>;
                }
            },
        },
        {
            title: 'Hình thức nhận',
            render: (_, record) => (
                <div className="text-sm">
                    {record.type == RECEIVE_TYPE.RECEIVE_AT_HOME
                        ? 'Nhận tại nhà'
                        : 'Nhận tại cửa hàng'}
                </div>
            ),
        },
    ];
    useEffect(() => {
        const getData = async () => {
            if (role == 'admin') {
                const response = await getListOrderAdmin({ status, page: 1, limit: 9999 });
                setListOrder(response.data);
            } else {
                const response = await getListOrder({ status, page: 1, limit: 9999 });
                setListOrder(response.data);
            }
        };
        getData();
    }, [role, status]);
    return (
        <>
            <Table dataSource={listOrder} columns={columns} onRow={(data, index) => {
                return {
                    onClick: (event) => {
                        if (role == 'admin') {
                            router.push("/admin/orders/" + data.id)
                        }
                        else router.push("/order/" + data.id)
                    }
                }
            }} />
        </>
    );
};

export default TableOrder;

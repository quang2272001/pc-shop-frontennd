import { Tabs, TabsProps } from 'antd';
import React from 'react'
import TableOrder from '../Table/TableOrder';
import { ORDER_STATUS } from '@/utils/constance';

const OrderTab = ({ role }: { role: 'admin' | 'user' }) => {

    const items: TabsProps['items'] = [
        {
            key: '1',
            label: 'Tất cả',
            children: <TableOrder status={ORDER_STATUS.all} role={role} />,
        },
        {
            key: '2',
            label: 'Chờ vận chuyển',
            children: <TableOrder status={ORDER_STATUS.created} role={role} />,
        },
        {
            key: '3',
            label: 'Đang vận chuyển',
            children: <TableOrder status={ORDER_STATUS.shipped} role={role} />,
        },
        {
            key: '4',
            label: 'Đã nhận',
            children: <TableOrder status={ORDER_STATUS.received} role={role} />,
        },
        {
            key: '5',
            label: 'Đã huỷ',
            children: <TableOrder status={ORDER_STATUS.cancled} role={role} />,
        },
    ];

    return (
        <>
            <h3 className="text-h4">Quản lí đơn hàng</h3>
            <Tabs className='mt-14' defaultActiveKey="1" items={items} />
        </>
    )
}

export default OrderTab
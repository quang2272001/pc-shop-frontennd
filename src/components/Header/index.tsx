import {
    Avatar,
    Badge,
    Button,
    Divider,
    Dropdown,
    MenuProps,
    Space,
} from 'antd';
import Svg from '../Svg';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import Link from 'next/link';
import { useState, useEffect } from 'react';
import { getUserInfo } from '@/utils/functions';
import { getListCatalog } from '@/service/api.service';
import { useRouter } from 'next/router';
import { ROLE } from '@/utils/constance';

type Props = {};

export default function Header({}: Props) {
    const router = useRouter();
    const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
    const [listCatalog, setlistCatalog] = useState<MenuProps['items']>([]);
    const [isAdmin, setIsAdmin] = useState(false);
    const [userId, setUserId] = useState('');

    useEffect(() => {
        const userInfo = getUserInfo();
        if (userInfo) {
            setIsAuthenticated(true);
        }
        if (
            userInfo?.user?.role == ROLE.ADMIN &&
            router.pathname.includes('admin')
        ) {
            setIsAdmin(true);
        }
    }, [router.pathname]);

    useEffect(() => {
        if (isAuthenticated) {
            getListCatalog({ limit: 9999, page: 1 }).then(response => {
                setlistCatalog(
                    response.data.map(d => {
                        return {
                            label: (
                                <Link href={'/catalog/' + d.id}>{d.name}</Link>
                            ),
                            key: d.id,
                        };
                    })
                );
            });
        }
    }, [isAuthenticated]);

    return (
        <>
            <div className="h-[92px] bg-white-0">
                <div
                    className="shadow-header flex justify-between items-center p-4"
                    style={{ borderBottom: '1px solid #CACDD8' }}
                >
                    <div className=" w-full md:px-28 lg:px-52 flex items-center justify-between ">
                        <div>
                            <Link
                                className="text-gray-100 whitespace-nowrap font-semibold leading-[30px]"
                                style={{ fontSize: 20 }}
                                href={'/'}
                            >
                                {' '}
                                PcStore
                            </Link>
                            {isAuthenticated && (
                                <>
                                    {router.pathname.includes('admin') ? (
                                        <Dropdown
                                            menu={{
                                                items: [
                                                    {
                                                        label: (
                                                            <Link href="/admin/products">
                                                                Quản lí sản phẩm
                                                            </Link>
                                                        ),
                                                        key: 1,
                                                    },
                                                    {
                                                        label: (
                                                            <Link href="/admin/orders">
                                                                Quản lí đơn hàng
                                                            </Link>
                                                        ),
                                                        key: 2,
                                                    },
                                                    {
                                                        label: (
                                                            <Link href="/admin/users">
                                                                Quản lí user
                                                            </Link>
                                                        ),
                                                        key: 3,
                                                    },
                                                ],
                                            }}
                                            trigger={['click']}
                                            className="ml-14"
                                        >
                                            <Button
                                                onClick={e =>
                                                    e.preventDefault()
                                                }
                                            >
                                                <Space className="flex items-center">
                                                    <div> Chức năng</div>
                                                    <DownOutlined />
                                                </Space>
                                            </Button>
                                        </Dropdown>
                                    ) : (
                                        <Dropdown
                                            menu={{ items: listCatalog }}
                                            trigger={['click']}
                                            className="ml-14"
                                        >
                                            <Button
                                                onClick={e =>
                                                    e.preventDefault()
                                                }
                                            >
                                                <Space className="flex items-center">
                                                    <div> Loại sản phẩm</div>
                                                    <DownOutlined />
                                                </Space>
                                            </Button>
                                        </Dropdown>
                                    )}
                                </>
                            )}
                        </div>
                        {isAuthenticated && (
                            <div className="flex gap-4 items-center">
                                {!router.pathname.includes('admin') && (
                                    <>
                                    <Link href="/search" >
                                        <Svg
                                            className="h-5 w-5 cursor-pointer "
                                            src="/images/icons/search.svg"
                                        />
                                    </Link>
                                        <Link href='/cart' >
                                        <Svg
                                            className="h-5 w-5 cursor-pointer "
                                            src="/images/icons/cart.svg"
                                            onClick={() => {
                                            }}
                                        />
                                        </Link>
                                    </>
                                )}

                                <Dropdown
                                    trigger={['click']}
                                    menu={{
                                        items: [
                                            {
                                                label: (
                                                    <div className="cursor-pointer">
                                                        Đăng xuất
                                                    </div>
                                                ),
                                                key: '0',
                                                onClick: () => {
                                                    localStorage.removeItem(
                                                        'authInfo'
                                                    );
                                                    router.push('/login');
                                                    setTimeout(() => {
                                                        router.reload();
                                                    }, 1000);
                                                },
                                            },
                                            {
                                                label: (
                                                    <Link
                                                        href="/order"
                                                        className="cursor-pointer"
                                                    >
                                                        Đơn hàng của bạn
                                                    </Link>
                                                ),
                                                key: '1',
                                            },
                                        ],
                                    }}
                                >
                                    <Avatar
                                        className="cursor-pointer"
                                        icon={<UserOutlined />}
                                    />
                                </Dropdown>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
}

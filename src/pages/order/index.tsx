import OrderTab from '@/components/Tab/OrderTab';
import { getListOrder, getListOrderAdmin } from '@/service/api.service';
import React, { useEffect } from 'react';

const OrderManager = () => {
    return <div>
        <OrderTab role='user' />
    </div>;
};

export default OrderManager;

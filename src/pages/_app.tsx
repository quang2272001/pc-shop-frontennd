import '@/styles/globals.scss'
import "@/styles/index.scss";
import React from 'react';
import { ConfigProvider } from 'antd';
import type { AppProps } from 'next/app';
import theme from '@/theme/themeConfig';
import { ErrorBoundary } from 'next/dist/client/components/error-boundary';
import Error from '@/error/error';
import DefaultLayout from '@/layouts/DefaultLayout';

const App = ({ Component, pageProps }: AppProps) => (
  <ErrorBoundary errorComponent={Error}>
    <DefaultLayout>
      <ConfigProvider theme={theme}>
        <Component {...pageProps} />
      </ConfigProvider>
    </DefaultLayout>
  </ErrorBoundary>
);

export default App;
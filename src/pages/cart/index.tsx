import TableProduct from '@/components/Table/TableProduct';
import { createOrder } from '@/service/api.service';
import { AddtoCartPayload, OrderPayload } from '@/types/payload.type';
import { RECEIVE_TYPE } from '@/utils/constance';
import { converMoney, getUserInfo } from '@/utils/functions';
import { showCustomNotification } from '@/utils/notification';
import { Button, Checkbox, Col, Divider, Input, Radio, Row, Space } from 'antd';
import React, { useEffect, useState } from 'react';

const CartComponent = () => {
    const initOrderPayload = {
        addressReceiver: '',
        nameReceiver: '',
        phoneReceiver: '',
        type: RECEIVE_TYPE.RECEIVE_AT_HOME,
        productInfos: [],
    };
    const [orderPayload, setOrderPayload] =
        useState<OrderPayload>(initOrderPayload);
    const [orderTotal, setOrderTotal] = useState(0);
    const [deleteProduct, setDeleteProduct] = useState<AddtoCartPayload[]>([]);

    useEffect(() => {
        const userInfo = getUserInfo().user;
        setOrderPayload({
            ...orderPayload,
            addressReceiver: userInfo.address,
            nameReceiver: userInfo.fullname,
            phoneReceiver: userInfo.phoneNumber,
        });
    }, []);

    const handleOrder = async () => {
        try {
            await createOrder(orderPayload);
            const userInfo = getUserInfo().user;
            setOrderPayload({
                ...initOrderPayload,
                addressReceiver: userInfo.address,
                nameReceiver: userInfo.fullname,
                phoneReceiver: userInfo.phoneNumber,
            });
            setDeleteProduct(orderPayload.productInfos);
            setOrderTotal(0);
            showCustomNotification({
                status: 'success',
                message: 'Thành công',
                description: 'Đơn hàng đã được đặt!',
            });
        } catch (error) {
            showCustomNotification({
                status: 'error',
                message: 'Lỗi',
                description: 'Đã có lỗi xảy ra!',
            });
        }
    };

    return (
        <div>
            <h3 className="font-medium text-h5">Giỏ hàng</h3>
            <Row gutter={20} className="mt-10">
                <Col md={16}>
                    <TableProduct
                        setOrderPayload={setOrderPayload}
                        setOrderTotal={setOrderTotal}
                        deleteProduct={deleteProduct}
                        setDeleteProduct={setDeleteProduct}
                    />
                </Col>
                <Col className="bg-[#F5F7FF] rounded-md" md={8}>
                    <div className="p-3">
                        <div className="text-h5 font-medium mb-10 ">
                            Thông tin vận chuyển
                        </div>
                        <Radio.Group
                            onChange={e => {
                                setOrderPayload({
                                    ...orderPayload,
                                    type: e.target.value,
                                });
                            }}
                            value={orderPayload.type}
                        >
                            <Radio value={RECEIVE_TYPE.RECEIVE_AT_HOME}>
                                <div className="text-sm"> Nhận tại nhà</div>
                            </Radio>
                            <Radio value={RECEIVE_TYPE.RECEIVE_AT_STORE}>
                                <div className="text-sm">Nhận tại cửa hàng</div>
                            </Radio>
                        </Radio.Group>

                        <div className="mt-10">
                            <div className="mb-1">Tên người nhận</div>
                            <Input
                                size="large"
                                value={orderPayload.nameReceiver}
                                onChange={e => {
                                    setOrderPayload({
                                        ...orderPayload,
                                        nameReceiver: e.target.value,
                                    });
                                }}
                            />
                        </div>
                        <div className="mt-6">
                            <div className="mb-1">Địa chỉ người nhận</div>
                            <Input
                                size="large"
                                value={orderPayload.addressReceiver}
                                onChange={e => {
                                    setOrderPayload({
                                        ...orderPayload,
                                        addressReceiver: e.target.value,
                                    });
                                }}
                            />
                        </div>
                        <div className="mt-6">
                            <div className="mb-1">Số điện thoại người nhận</div>
                            <Input
                                size="large"
                                value={orderPayload.phoneReceiver}
                                onChange={e => {
                                    setOrderPayload({
                                        ...orderPayload,
                                        phoneReceiver: e.target.value,
                                    });
                                }}
                            />
                        </div>
                        <Divider />
                        <Row>
                            <Col span={12}>Tổng tiền:</Col>
                            <Col span={12}>{converMoney(orderTotal)}</Col>
                        </Row>
                        <Button
                            type="text"
                            style={{
                                backgroundColor: '#0156FF',
                                color: 'white',
                                width: '100%',
                                borderRadius: '50px',
                                marginTop: 50,
                                marginBottom: 50,
                            }}
                            disabled={orderPayload.productInfos.length < 1}
                            onClick={() => {
                                handleOrder();
                            }}
                        >
                            Đặt hàng
                        </Button>
                    </div>
                </Col>
            </Row>
        </div>
    );
};

export default CartComponent;

import { createUser } from '@/service/api.service';
import { User } from '@/types/response.type';
import { ROLE } from '@/utils/constance';
import { handleException } from '@/utils/functions';
import { showCustomNotification } from '@/utils/notification';
import { Button, Input, Row } from 'antd';
import { useRouter } from 'next/router';
import React, { useState } from 'react';

const Register = () => {
    const initUser = {
        address: '',
        email: '',
        fullname: '',
        phoneNumber: '',
        role: ROLE.USER,
        username: '',
        password: '',
    };
    const [userInfo, setUserInfo] = useState<User>(initUser);
    const router = useRouter();

    const register = async () => {
        try {
            await createUser({
                ...userInfo,
                password: String(userInfo.password),
            });
            showCustomNotification({
                status: 'success',
                message: 'Thành công',
                description: 'Đăng ký thành công',
            });
        } catch (error) {
            handleException(error);
        }
    };

    return (
        <div>
            <h3 className="text-h4 text-center">Đăng ký</h3>
            <div className="flex justify-center">
                <div className="w-1/2 ">
                    <Row className="mt-6">
                        <label className="mb-2">Email *</label>
                        <Input
                            size="large"
                            type="email"
                            value={userInfo.email}
                            onChange={e => {
                                setUserInfo({
                                    ...userInfo,
                                    email: e.target.value,
                                });
                            }}
                            placeholder="Email"
                        />
                    </Row>
                    <Row className="mt-6">
                        <label className="mb-2">Password *</label>
                        <Input
                            size="large"
                            type="password"
                            placeholder="Password"
                            value={userInfo.password}
                            onChange={e => {
                                setUserInfo({
                                    ...userInfo,
                                    password: e.target.value,
                                });
                            }}
                        />
                    </Row>
                    <Row className="mt-6">
                        <label className="mb-2">UserName *</label>
                        <Input
                            size="large"
                            placeholder="UserName"
                            value={userInfo.username}
                            onChange={e => {
                                setUserInfo({
                                    ...userInfo,
                                    username: e.target.value,
                                });
                            }}
                        />
                    </Row>
                    <Row className="mt-6">
                        <label className="mb-2">Họ tên *</label>
                        <Input
                            size="large"
                            placeholder="Họ tên"
                            value={userInfo.fullname}
                            onChange={e => {
                                setUserInfo({
                                    ...userInfo,
                                    fullname: e.target.value,
                                });
                            }}
                        />
                    </Row>
                    <Row className="mt-6">
                        <label className="mb-2">Địa chỉ *</label>
                        <Input
                            size="large"
                            placeholder="Địa chỉ"
                            value={userInfo.address}
                            onChange={e => {
                                setUserInfo({
                                    ...userInfo,
                                    address: e.target.value,
                                });
                            }}
                        />
                    </Row>
                    <Row className="mt-4">
                        <label className="mb-2">Số điện thoại *</label>
                        <Input
                            size="large"
                            placeholder="Địa chỉ"
                            value={userInfo.phoneNumber}
                            onChange={e => {
                                setUserInfo({
                                    ...userInfo,
                                    phoneNumber: e.target.value,
                                });
                            }}
                        />
                    </Row>
                    <Row justify={'center'} className="mt-10">
                        <Button
                            size="large"
                            className="w-1/2"
                            onClick={() => {
                                register();
                                router.push('/login');
                            }}
                            type="primary"
                        >
                            Đăng ký
                        </Button>
                    </Row>
                </div>
            </div>
        </div>
    );
};

export default Register;

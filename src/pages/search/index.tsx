import { getListProduct } from '@/service/api.service';
import { Product } from '@/types/response.type';
import { converMoney } from '@/utils/functions';
import { Card, Input, List } from 'antd';
import Link from 'next/link';
import React, { useState } from 'react';

const { Search } = Input;

const SearchComponent = () => {
    const [listProduct, setListProduct] = useState<Product[]>([]);
    const [searchValue, setSearchValue] = useState('');
    const onSearch = () => {
        getListProduct({ keyword: searchValue, page: 1, limit: 9999 }).then(
            response => {
                setListProduct(response.data);
            }
        );
    };
    return (
        <div>
            <h3 className="font-medium text-h5">Tìm kiếm</h3>
            <div className="mt-36 flex justify-center">
                <Search
                    placeholder="input search text"
                    allowClear
                    className="md:px-40"
                    enterButton="Search"
                    value={searchValue}
                    onChange={e => {
                        setSearchValue(e.target.value);
                    }}
                    size="large"
                    onSearch={onSearch}
                />
            </div>
            <List
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 2,
                    lg: 4,
                }}
                pagination={{
                    onChange: page => {
                        console.log(page);
                    },
                    pageSize: 10,
                    responsive: true,
                    align: 'center',
                }}
                dataSource={listProduct}
                renderItem={item => (
                    <Link href={'/product/' + item.id}>
                        <List.Item>
                            <Card
                                hoverable
                                style={{ width: 240, height: 400 }}
                                cover={
                                    // eslint-disable-next-line @next/next/no-img-element
                                    <img alt="example" src={item.images[0]} />
                                }
                            >
                                <div>
                                    <div className="text-xs">{item.name}</div>
                                    <div className="mt-2 text-base text-gray-400">
                                        {converMoney(item.price)}
                                    </div>
                                </div>
                            </Card>
                        </List.Item>
                    </Link>
                )}
            />
        </div>
    );
};

export default SearchComponent;

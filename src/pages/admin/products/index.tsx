import { deleteProduct, getListProduct } from '@/service/api.service';
import { Product } from '@/types/response.type';
import { converMoney } from '@/utils/functions';
import { showCustomNotification } from '@/utils/notification';
import { PlusOutlined } from '@ant-design/icons';
import { Button, Input, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useState } from 'react';

const ProductManger = () => {
    const [listProduct, setListProduct] = useState<Product[]>([]);
    const [keyword, setKeyword] = useState<string>('');
    const [openCreateProductModal, setOpenCreateProductModal] = useState(false)
    const [openDeleteModal, setOpenDeleteModal] = useState(false)
    const [reloadData, setReloadData] = useState(false)
    const [productId, setProductId] = useState('');
    const router = useRouter();
    const CreateProductModal = dynamic(
        () => import('@/components/Modal/CreateProduct'),
        { ssr: false }
    );
    const DeleteModal = dynamic(
        () => import('@/components/Modal/DeleteModal'),
        { ssr: false }
    );

    useEffect(() => {
        getListProduct({ keyword: keyword, page: 1, limit: 9999 }).then(
            response => {
                setListProduct(response.data);
            }
        );
    }, [keyword, reloadData]);

    const columns: ColumnsType<Product> = [
        {
            title: 'Ảnh',
            key: 'image',
            render: (value, record) => (
                <div>
                    <img src={record.images[0]} width={120} height={120} />
                </div>
            ),
            width: '20%',
        },
        {
            title: 'Tên',
            dataIndex: 'name',
            key: 'name',
            render: text => <div className="text-sm">{text}</div>,
            width: '20%',
        },
        {
            title: 'Giá',
            dataIndex: 'price',
            key: 'price',
            width: '20%',
            render: price => (
                <div className="text-base text-gray-400">
                    {converMoney(price)}
                </div>
            ),
        },
        {
            title: 'Còn lại',
            width: '10%',
            dataIndex: 'quantity',
            key: 'quantity',
        },
        {
            title: 'Thao tác',
            key: 'action',
            render: (_, record) => (
                <div className="flex gap-3">
                    <Button onClick={() => {
                        setProductId(String(record.id));
                        setOpenCreateProductModal(true);
                    }} >Sửa</Button>
                    <Button onClick={() => {
                        setProductId(String(record.id));
                        setOpenDeleteModal(true);
                    }} className="text-red-500">Xoá</Button>
                </div>
            ),
        },
    ];

    const handleDeleteProduct = useCallback(async () => {
        try {
            await deleteProduct(productId);
            setReloadData(!reloadData);
            setOpenDeleteModal(false);
            setProductId('');
        } catch (error) {
            showCustomNotification({
                status: 'error',
                message: 'Lỗi',
                description: 'Đã có lỗi xảy ra!',
            });
        }
    }, [productId])

    const handelCancelModalProduct = () => {
        setOpenCreateProductModal(false)
        setProductId('');
    }

    return (
        <>
            <h3 className="text-h4">Quản lí sản phẩm</h3>
            <div className="mt-12 flex justify-between ">
                <Button type="primary" onClick={() => { setOpenCreateProductModal(true) }} icon={<PlusOutlined />}>
                    Thêm sản phẩm
                </Button>

                <div>
                    <Input
                        className="rounded-xl"
                        placeholder="Nhập tên sản phẩm"
                        onChange={e => {
                            setTimeout(() => {
                                setKeyword(e.target.value);
                            }, 500);
                        }}
                    />
                </div>
            </div>
            <Table
                className="mt-10"
                dataSource={listProduct}
                columns={columns}
            />
            <CreateProductModal productId={productId}
                isModalOpen={openCreateProductModal}
                handleOk={() => { setProductId(''); setOpenCreateProductModal(false); setReloadData(!reloadData); }}
                handleCancle={handelCancelModalProduct} />
            <DeleteModal handleCancle={() => {
                setOpenDeleteModal(false)
                setProductId('');
            }} isModalOpen={openDeleteModal} title='Bạn chắc chắn muốn xoá sản phẩm này?' handleOk={handleDeleteProduct} productId={productId} />
        </>
    );
};

export default ProductManger;

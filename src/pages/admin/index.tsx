import { ROLE } from '@/utils/constance';
import { getUserInfo } from '@/utils/functions';
import { Card, Col, List, Row } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';

const AdminHome = () => {
    const router = useRouter();
    useEffect(() => {
        const loginInfo = getUserInfo();
        if (loginInfo.user.role !== ROLE.ADMIN) {
            router.push('/');
            router.reload;
        }
    }, []);

    return (
        <div>
            <Row className="mt-40" gutter={32}>
                <Col span={8}>
                    <Link href="admin/products">
                        <Card
                            className="h-32 flex justify-center items-center "
                            title=""
                            bordered={true}
                        >
                            Quản lí sản phẩm
                        </Card>
                    </Link>
                </Col>
                <Col span={8}>
                    <Link href="admin/orders">
                        <Card
                            className="h-32 flex justify-center items-center"
                            title=""
                            bordered={true}
                        >
                            Quản lí đơn hàng
                        </Card>
                    </Link>
                </Col>
                <Col span={8}>
                    <Link href="admin/users">
                        <Card
                            className="h-32 flex justify-center items-center "
                            title=""
                            bordered={true}
                        >
                            Quản lí user{' '}
                        </Card>
                    </Link>
                </Col>
            </Row>
        </div>
    );
};

export default AdminHome;

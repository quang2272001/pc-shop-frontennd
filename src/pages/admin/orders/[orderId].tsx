import { OrderDetailComponent } from '@/components/Order/OrderDetailComponent'
import { ROLE } from '@/utils/constance'
import React from 'react'

const OrderDetail = () => {
    return (
        <OrderDetailComponent role={ROLE.ADMIN} />
    )
}

export default OrderDetail
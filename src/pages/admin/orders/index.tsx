import OrderTab from '@/components/Tab/OrderTab';
import React, { useEffect } from 'react';

const OrderManager = () => {
    return <div>
        <OrderTab role='admin' />
    </div>;
};

export default OrderManager;

import { getListUser, getUserDetail } from '@/service/api.service';
import { User } from '@/types/response.type';
import { ROLE } from '@/utils/constance';
import { PlusOutlined } from '@ant-design/icons';
import { Button, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import dynamic from 'next/dynamic';
import React, { useEffect, useState } from 'react';

const UserManager = () => {
    const [listUser, setListUser] = useState<User[]>([]);
    const [userId, setUserId] = useState('');
    const [openCreateUserModal, setOpenCreateUserModal] = useState(false);
    const [reloadData, setReloadData] = useState(false);

    const CreateUpdateUserModal = dynamic(
        () => import('@/components/Modal/CreateUpdateUser'),
        { ssr: false }
    );

    useEffect(() => {
        const getData = async () => {
            const response = await getListUser({ page: 1, limit: 9999 });
            setListUser(response.data);
        };
        getData();
    }, [reloadData]);

    const columns: ColumnsType<User> = [
        { title: 'Tên', dataIndex: 'fullname' },
        { title: 'Email', dataIndex: 'email' },
        { title: 'Địa chỉ', dataIndex: 'address' },
        { title: 'Số điện thoại', dataIndex: 'phoneNumber' },
        {
            title: 'Vai trò',
            dataIndex: 'role',
            render(value, record, index) {
                return value == ROLE.ADMIN ? 'Admin' : 'User';
            },
        },
    ];

    return (
        <div>
            <div className="mt-5 text-h4">Quản lí User</div>
            <div className="mt-12 flex justify-between ">
                <Button
                    type="primary"
                    onClick={() => {
                        setOpenCreateUserModal(true);
                    }}
                    icon={<PlusOutlined />}
                >
                    Thêm User
                </Button>
            </div>
            <Table
                className="mt-10"
                columns={columns}
                onRow={(data, index) => {
                    return {
                        onClick(e) {
                            setUserId(String(data.id));
                            setOpenCreateUserModal(true);
                        },
                    };
                }}
                dataSource={listUser.map((user, index) => {
                    return {
                        ...user,
                        key: index,
                    };
                })}
            />
            <CreateUpdateUserModal
                handleCancle={() => {
                    setOpenCreateUserModal(false);
                    setUserId('');
                }}
                handleOk={() => {
                    setOpenCreateUserModal(false);
                    setUserId('');
                    setReloadData(!reloadData);
                }}
                isModalOpen={openCreateUserModal}
                userId={userId}
            />
        </div>
    );
};

export default UserManager;

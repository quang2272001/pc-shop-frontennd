import Image from 'next/image';
import { Inter } from 'next/font/google';
import React, { useEffect, useState } from 'react';
import { getListCatalog, getListProduct } from '@/service/api.service';
import { Card, List } from 'antd';
import { Catalog, Product } from '@/types/response.type';
import Link from 'next/link';
import { showCustomNotification } from '@/utils/notification';
import { converMoney } from '@/utils/functions';
const inter = Inter({ subsets: ['latin'] });

const { Meta } = Card;
export default function Home() {
    const [listProduct, setListProduct] = useState<Product[]>([]);
    const [listCatalog, setListCatalog] = useState<Catalog[]>([]);
    useEffect(() => {
        const getData = async () => {
            try {
                const responseProduct = await getListProduct({
                    limit: 9999,
                    page: 1,
                });
                setListProduct(responseProduct.data as Product[]);
                const responseCatalog = await getListCatalog({
                    page: 1,
                    limit: 9999,
                });
                setListCatalog(responseCatalog.data as Catalog[]);
            } catch (error) {
                // showCustomNotification({
                //     status: 'error',
                //     message: 'Lỗi',
                //     description: 'Đã có lỗi xảy ra!',
                // });
            }
        };
        getData();
    }, []);
    return (
        <>
            <div className="flex flex-col gap-5">
                <div>
                    <div className="flex justify-between">
                        <h4 className="font-semibold mb-3">Sản phẩm mới</h4>
                        <Link href="/search" className="text-sm">
                            xem tất cả sản phẩm
                        </Link>
                    </div>
                    <List
                        grid={{
                            gutter: 16,
                            xs: 1,
                            sm: 2,
                            md: 2,
                            lg: 4,
                        }}
                        dataSource={listProduct.slice(0, 4)}
                        renderItem={item => (
                            <Link href={'/product/' + item.id}>
                                <List.Item>
                                    <Card
                                        hoverable
                                        style={{ width: 240, height: 400 }}
                                        cover={
                                            // eslint-disable-next-line @next/next/no-img-element
                                            <img
                                                alt="example"
                                                src={item.images[0]}
                                            />
                                        }
                                    >
                                        <div>
                                            <div className="text-xs">
                                                {item.name}
                                            </div>
                                            <div className="mt-2 text-base text-gray-400">
                                                {converMoney(item.price)}
                                            </div>
                                        </div>
                                    </Card>
                                </List.Item>
                            </Link>
                        )}
                    />
                </div>
                <div>
                    <div className="flex justify-between">
                        <h4 className="font-semibold mb-3">Loại sản phẩm</h4>
                    </div>
                    <List
                        grid={{
                            gutter: 16,
                            xs: 1,
                            sm: 2,
                            md: 2,
                            lg: 4,
                        }}
                        pagination={{
                            onChange: page => {
                                console.log(page);
                            },
                            pageSize: 4,
                            align: 'center',
                        }}
                        dataSource={listCatalog}
                        renderItem={item => (
                            <Link href={'/catalog/' + item.id}>
                                <List.Item>
                                    <Card
                                        hoverable
                                        style={{ width: 240, height: 250 }}
                                        cover={
                                            // eslint-disable-next-line @next/next/no-img-element
                                            <img
                                                alt="example"
                                                src={item.image}
                                            />
                                        }
                                    >
                                        <div>
                                            <div className="text-xs">
                                                {item.name}
                                            </div>
                                        </div>
                                    </Card>
                                </List.Item>
                            </Link>
                        )}
                    />
                </div>
                <div>
                    <h4 className="font-semibold mb-3">Tất cả sản phẩm</h4>
                    <List
                        grid={{
                            gutter: 16,
                            xs: 1,
                            sm: 2,
                            md: 2,
                            lg: 4,
                        }}
                        pagination={{
                            onChange: page => {
                                console.log(page);
                            },
                            pageSize: 10,
                            responsive: true,
                            align: 'center',
                        }}
                        dataSource={listProduct}
                        renderItem={item => (
                            <Link href={'/product/' + item.id}>
                                <List.Item>
                                    <Card
                                        hoverable
                                        style={{ width: 240, height: 400 }}
                                        cover={
                                            // eslint-disable-next-line @next/next/no-img-element
                                            <img
                                                alt="example"
                                                src={item.images[0]}
                                            />
                                        }
                                    >
                                        <div>
                                            <div className="text-xs">
                                                {item.name}
                                            </div>
                                            <div className="mt-2 text-base text-gray-400">
                                                {converMoney(item.price)}
                                            </div>
                                        </div>
                                    </Card>
                                </List.Item>
                            </Link>
                        )}
                    />
                </div>
            </div>
        </>
    );
}

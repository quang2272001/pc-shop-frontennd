import { addToCart, getDetailProduct } from '@/service/api.service';
import { AddtoCartPayload } from '@/types/payload.type';
import { Product } from '@/types/response.type';
import { converMoney } from '@/utils/functions';
import { showCustomNotification } from '@/utils/notification';
import { ShoppingCartOutlined } from '@ant-design/icons';
import { Button, Carousel, Col, Image, InputNumber, Row } from 'antd';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
const contentStyle: React.CSSProperties = {
    height: '45vh',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: 'black',
};

const ProductDetail = () => {
    const router = useRouter();
    const { productId } = router.query;
    const [detailProduct, setDetailProduct] = useState<Product>({
        description: '',
        id: 0,
        images: [],
        name: '',
        price: 0,
        properties: '',
        quantity: 0,
        catalogIds: [],
    });
    const [cartPayload, setCartPayload] = useState<AddtoCartPayload>({
        id: 0,
        quantity: 1,
    });
    const [properties, setProperties] = useState<string[]>([]);
    useEffect(() => {
        const getData = async () => {
            try {
                const response = await getDetailProduct(productId as string);
                setDetailProduct(response.data);
                setProperties(response.data.properties.split('|'));
            } catch (error) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: 'Đã có lỗi xảy ra!',
                });
            }
        };
        if (productId) {
            setCartPayload({ id: Number(productId), quantity: 1 });
            getData();
        }
    }, [productId]);
    const handleAddToCart = async () => {
        try {
            await addToCart(cartPayload);
            showCustomNotification({
                status: 'success',
                message: 'Thành công',
                description: 'Sản phẩm đã được thêm vào giỏ hàng!',
            });
        } catch (error) {
            showCustomNotification({
                status: 'error',
                message: 'Lỗi',
                description: 'Đã có lỗi xảy ra!',
            });
        }
    };
    return (
        <div>
            <Row>
                <Col xs={24} xl={12}>
                    {' '}
                    <Carousel className="lg:w-10/12 md:w-full" autoplay>
                        {detailProduct.images.map(image => (
                            <div key={image}>
                                <div style={contentStyle} className="w-full">
                                    <Image
                                        height={'40vh'}
                                        width={'100%'}
                                        src={image}
                                        alt=""
                                    />
                                </div>
                            </div>
                        ))}
                    </Carousel>
                </Col>
                <Col xs={24} xl={12}>
                    <Row className="mt-3">
                        <h3 className="font-medium">{detailProduct.name}</h3>
                    </Row>
                    <Row className="mt-2">
                        <Col xs={24} xl={8} className="text-gray-600">
                            giá:{' '}
                            <span className="font-medium">
                                {converMoney(detailProduct.price)}
                            </span>
                        </Col>
                        <Col xs={24} xl={8} className="text-gray-600">
                            còn lại:{' '}
                            <span className="font-medium">
                                {detailProduct.quantity}
                            </span>
                        </Col>
                    </Row>
                    <Row className="mt-10">
                        {properties.map(propertie => (
                            <div key={propertie} className="mt-2">
                                {propertie}
                            </div>
                        ))}
                    </Row>
                    <Row className="mt-10 flex flex-wrap gap-5 items-center">
                        <div>Số lượng</div>
                        <InputNumber
                            min={1}
                            max={100000}
                            value={cartPayload.quantity}
                            onChange={e => {
                                if (Number(e)) {
                                    setCartPayload({
                                        ...cartPayload,
                                        quantity: e as number,
                                    });
                                }
                            }}
                        />
                    </Row>
                    <Row className="mt-5">
                        <Button
                            icon={<ShoppingCartOutlined />}
                            size="large"
                            type="primary"
                            onClick={() => {
                                handleAddToCart();
                            }}
                        >
                            Thêm vào giỏ hàng
                        </Button>
                    </Row>
                </Col>
            </Row>
            <div className="mt-24">
                <h3>MÔ TẢ SẢN PHẨM</h3>
                <div className="mt-5 text-base">
                    {detailProduct.description}
                </div>
            </div>
        </div>
    );
};

export default ProductDetail;

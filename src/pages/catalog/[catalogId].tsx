import { getDetailCatalog, getListProduct } from '@/service/api.service';
import { Catalog, Product } from '@/types/response.type';
import { showCustomNotification } from '@/utils/notification';
import { Card, List } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

const CatalogComponent = () => {
    const router = useRouter();
    const { catalogId } = router.query;
    const [listProduct, setListProduct] = useState<Product[]>([]);
    const [detailCatalog, setDetailCatalog] = useState<Catalog>({
        description: '',
        id: 0,
        image: '',
        name: '',
    });

    useEffect(() => {
        const getData = async () => {
            try {
                const responseProduct = await getListProduct({
                    catalogId: Number(catalogId),
                    limit: 9999, page: 1
                });
                setListProduct(responseProduct.data as Product[]);
                const responseDetailCatalog = await getDetailCatalog(
                    String(catalogId)
                );
                setDetailCatalog(responseDetailCatalog.data as Catalog);
            } catch (error) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: 'Đã có lỗi xảy ra!',
                });
            }
        };
        if (catalogId) getData();
    }, [catalogId]);

    return (
        <div>
            <h4 className="font-semibold mb-3">
                Các sản phẩm thuộc loại {detailCatalog.name}{' '}
            </h4>
            <div className="text-base mb-10">{detailCatalog.description}</div>
            <List
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 2,
                    lg: 4,
                }}
                pagination={{
                    onChange: page => {
                        console.log(page);
                    },
                    pageSize: 8,
                    align: 'center',
                }}
                dataSource={listProduct}
                renderItem={item => (
                    <Link href={'/product/' + item.id}>
                        {' '}
                        <List.Item>
                            <Card
                                hoverable
                                style={{ width: 240, height: 400 }}
                                cover={
                                    // eslint-disable-next-line @next/next/no-img-element
                                    <img alt="example" src={item.images[0]} />
                                }
                            >
                                <div className="text-xs">{item.name}</div>
                                <div className="mt-2 text-base text-gray-400">
                                    {new Intl.NumberFormat('vi-VN', {
                                        style: 'currency',
                                        currency: 'VND',
                                    }).format(Number(item.price))}
                                </div>
                            </Card>
                        </List.Item>
                    </Link>
                )}
            />
        </div>
    );
};

export default CatalogComponent;

import { login } from '@/service/api.service';
import { LoginPayload } from '@/types/payload.type';
import { ERROR_MESSAGE } from '@/utils/constance';
import { getUserInfo, validateData } from '@/utils/functions';
import { showCustomNotification } from '@/utils/notification';
import { Button, Col, Input, Row } from 'antd';
import React, { useState } from 'react';
import { useRouter } from 'next/router';
import axiosInstance from '@/utils/axios';

const LoginComponent = () => {
    const [loginData, setLoginData] = useState<LoginPayload>({
        email: '',
        password: '',
    });

    const router = useRouter();

    const handleLogin = async () => {
        try {
            const validate = await validateData(
                new LoginPayload({ ...loginData })
            );
            if (!validate) return;
            const response = await login(loginData);
            console.log(response);
            axiosInstance.defaults.headers.Authorization = `Bear ${response.data?.access_token}`;
            console.log(axiosInstance.defaults.headers.Authorization);
            localStorage.setItem('authInfo', JSON.stringify(response.data));
            router.push('/');
            router.reload();
        } catch (error: any) {
            if (error?.response?.data?.statusCode == 401) {
                showCustomNotification({
                    status: 'error',
                    message: 'Lỗi',
                    description: ERROR_MESSAGE[error?.response?.data?.message],
                });
                return;
            }
            showCustomNotification({
                status: 'error',
                message: 'Lỗi',
                description: 'Đã có lỗi xảy ra!',
            });
        }
    };
    return (
        <div className="mt-14">
            <div className="text-h5 font-semibold mb-2">Đăng nhập</div>
            <Row justify={'center'} gutter={50} className="mt-5">
                <Col
                    xs={24}
                    md={12}
                    className="flex flex-col gap-6 justify-center items-center p-8 bg-[#F5F7FF] "
                >
                    <Row className="text-h-xxs font-semibold">
                        {' '}
                        Khách hàng đã đăng ký
                    </Row>
                    <Row className="mt-6">
                        Nếu bạn có tài khoản, hãy đăng nhập bằng địa chỉ email
                        của bạn.
                    </Row>
                    <Row className="mt-6">
                        <label className="mb-2">Email *</label>
                        <Input
                            size="large"
                            type="email"
                            value={loginData.email}
                            onChange={e => {
                                setLoginData({
                                    ...loginData,
                                    email: e.target.value,
                                });
                            }}
                            placeholder="Email"
                        />
                    </Row>
                    <Row className="mt-4">
                        <label className="mb-2">Password *</label>
                        <Input
                            size="large"
                            type="password"
                            placeholder="Password"
                            value={loginData.password}
                            onChange={e => {
                                setLoginData({
                                    ...loginData,
                                    password: e.target.value,
                                });
                            }}
                        />
                    </Row>
                    <Row className="mt-4 justify-center">
                        <Button
                            type="primary"
                            size="large"
                            onClick={handleLogin}
                        >
                            {' '}
                            Đăng Nhập
                        </Button>
                    </Row>
                </Col>
                <Col xs={24} md={12} className="w-full flex justify-center ">
                    <Row className="bg-[#F5F7FF] w-full flex flex-col gap-6 p-8">
                        <Row className="text-h-xxs font-semibold">
                            Khách hàng mới?
                        </Row>
                        <Row>
                            Việc tạo tài khoản có rất nhiều lợi ích:
                            <ul className="ml-4 mt-2">
                                <Row> Thanh toán nhanh hơn</Row>
                                <Row>Giữ nhiều địa chỉ</Row>
                                <Row>Theo dõi đơn hàng và hơn thế nữa</Row>
                            </ul>
                        </Row>
                        <Row justify="center" className="w-full">
                            <Button
                                type="primary"
                                onClick={() => {
                                    router.push('/register');
                                }}
                                size="large"
                            >
                                Đăng ký
                            </Button>
                        </Row>
                    </Row>
                </Col>
            </Row>
        </div>
    );
};

export default LoginComponent;

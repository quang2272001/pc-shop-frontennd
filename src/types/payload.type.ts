import { RECEIVE_TYPE, ROLE } from '@/utils/constance';
import { UploadFile } from 'antd';
import { RcFile } from 'antd/lib/upload';
import { IsEmail, IsEnum, IsNotEmpty, IsNumberString } from 'class-validator';

export class LoginPayload {
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsNotEmpty({ message: 'adifnklsdf' })
    password: string;

    constructor(params: LoginPayload) {
        this.email = params.email;
        this.password = params.password;
    }
}

export class AddtoCartPayload {
    id: number;
    quantity: number;
}

export class OrderPayload {
    nameReceiver: string;

    addressReceiver: string;

    phoneReceiver: string;

    type: RECEIVE_TYPE;

    productInfos: AddtoCartPayload[];
}

export interface CreateProductPayload {
    name: string;
    price: number;
    description: string;
    quantity: number;
    properties: string;
    images: File[] | string[];
    catalogIds: number[];
}

export interface UpdateProductPayload {
    name: string;
    price: number;
    description: string;
    quantity: number;
    properties: string;
    catalogIds: number[];
}


export interface CreateUserPayload {
    address: string;
    email: string;
    fullname: string;
    phoneNumber: string;
    role: ROLE;
    username: string;
    password: string;
}

export interface UpdateUserPayload {
    address: string;
    email: string;
    fullname: string;
    phoneNumber: string;
    role: ROLE;
    username: string;
}
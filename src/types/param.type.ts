import { ORDER_STATUS } from "@/utils/constance";

export interface FindAllParam {
    page?: number;
    limit?: number;
    keyword?: string;
    sort?: string;
}

export interface FindAllProductParam extends FindAllParam {
    catalogId?: number;
}

export interface FindAllOrderParam extends FindAllParam {
    status?: ORDER_STATUS
}

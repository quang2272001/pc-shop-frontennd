import { ORDER_STATUS, ROLE } from '@/utils/constance';

export interface ApiResponse<T> {
    data: T;
    success: boolean;
}

export interface LoginResponse {
    access_token: string;
    user: {
        email: string;
        fullname: string;
        username: string;
        address: string;
        dob: string;
        phoneNumber: string;
        id: number;
        role: number;
    };
}

export interface Product {
    id: number;
    name: string;
    price: number;
    description: string;
    quantity: number;
    properties: string;
    images: string[];
    catalogIds: number[];
}

export interface Catalog {
    id: number;
    name: string;
    description: string;
    image: string;
}

export interface Cart {
    product: Product;
    quantity: number;
}

export interface Order {
    id: number;
    status: ORDER_STATUS;
    nameReceiver: string;
    addressReceiver: string;
    phoneReceiver: string;
    type: number;
    listProduct: Array<{ product: Product; quantity: number }>;
}

export interface User {
    id?: string;
    address: string;
    email: string;
    fullname: string;
    phoneNumber: string;
    role: ROLE;
    username: string;
    password?: string;
}

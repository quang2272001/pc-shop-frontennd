import { Cart, Order, User } from './../types/response.type';
import { FindAllOrderParam, FindAllParam, FindAllProductParam } from '@/types/param.type';
import {
    AddtoCartPayload,
    CreateProductPayload,
    CreateUserPayload,
    LoginPayload,
    OrderPayload,
    UpdateProductPayload,
    UpdateUserPayload,
} from '@/types/payload.type';
import {
    ApiResponse,
    Catalog,
    LoginResponse,
    Product,
} from '@/types/response.type';
import axiosInstance from '@/utils/axios';
import { ORDER_STATUS } from '@/utils/constance';

export async function login(
    payload: LoginPayload
): Promise<ApiResponse<LoginResponse>> {
    const response = await axiosInstance.post('api/v1/auth/login', payload);
    return response.data;
}

export async function getListProduct(
    params: FindAllProductParam
): Promise<ApiResponse<Product[]>> {
    const response = await axiosInstance.get('api/v1/product', { params });
    return response.data;
}

export async function getListCatalog(
    params: FindAllParam
): Promise<ApiResponse<Catalog[]>> {
    const response = await axiosInstance.get('api/v1/catalog', { params });
    return response.data;
}

export async function getDetailCatalog(
    catalogId: string
): Promise<ApiResponse<Catalog>> {
    const response = await axiosInstance.get('api/v1/catalog/' + catalogId);
    return response.data;
}

export async function getDetailProduct(
    productId: string
): Promise<ApiResponse<Product>> {
    const response = await axiosInstance.get('/api/v1/product/' + productId);
    return response.data;
}

export async function addToCart(payload: AddtoCartPayload) {
    return axiosInstance.post('/api/v1/product/cart/', payload);
}

export async function getCart(): Promise<ApiResponse<Cart[]>> {
    const response = await axiosInstance.get('/api/v1/product/cart/array');
    return response.data;
}

export async function updateCart(payload: { cart: AddtoCartPayload[] }) {
    return axiosInstance.patch('/api/v1/product/cart/update', payload);
}

export async function createOrder(payload: OrderPayload) {
    return axiosInstance.post('/api/v1/order', payload);
}

export async function getListOrder(params: FindAllOrderParam): Promise<ApiResponse<Order[]>> {
    const response = await axiosInstance.get('/api/v1/order/', { params });
    return response.data;
}

export async function getListOrderAdmin(params: FindAllOrderParam): Promise<ApiResponse<Order[]>> {
    const response = await axiosInstance.get('/api/v1/order/admin/getall/', { params });
    return response.data;
}

export async function getOrderDetail(orderId: string): Promise<ApiResponse<Order>> {
    const response = await axiosInstance.get('/api/v1/order/' + orderId);
    return response.data;
}

export async function updateOrder(orderId: string, updateOrderPayload: {
    status: ORDER_STATUS;
    nameReceiver: string;
    addressReceiver: string;
    phoneReceiver: string;
    type: number;
}) {
    const response = await axiosInstance.patch('/api/v1/order/' + orderId, updateOrderPayload);
}

export async function updateProduct(productId: string, payload: UpdateProductPayload) {
    return axiosInstance.patch('/api/v1/product/' + productId, payload);
}

export async function deleteProduct(productId: string) {
    return axiosInstance.delete('/api/v1/product/' + productId);
}

export async function createProduct(payload: CreateProductPayload) {
    const form = new FormData();
    form.append('name', payload.name);
    form.append('quantity', String(payload.quantity));
    form.append('catalogIds', String(payload.catalogIds));
    form.append('description', payload.description);
    form.append('properties', payload.properties);
    form.append('price', String(payload.price));
    for (const image of payload.images as File[]) {
        form.append('images', image);
    }
    return axiosInstance.post('/api/v1/product', form, {
        headers: {
            'Content-Type': 'multipart/form-data',
        },
    });
}

export async function getListUser(params: FindAllParam): Promise<ApiResponse<User[]>> {
    const response = await axiosInstance.get("/api/v1/user/", { params });
    return response.data;
}

export async function getUserDetail(userId: string): Promise<ApiResponse<User>> {
    const response = await axiosInstance.get('/api/v1/user/' + userId);
    return response.data;
}

export async function createUser(payload: CreateUserPayload) {
    return axiosInstance.post('/api/v1/user/', payload);
}

export async function updateUser(userId: string, payload: UpdateUserPayload) {
    return axiosInstance.patch('/api/v1/user/' + userId, payload);
}
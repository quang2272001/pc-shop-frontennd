
export interface ModalProps {
    handleOk: () => void;
    handleCancle: () => void;
    isModalOpen: boolean;
}
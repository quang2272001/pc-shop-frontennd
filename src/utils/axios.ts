import { LoginResponse } from '@/types/response.type';
import axios from 'axios';
import { getUserInfo } from './functions';

const axiosInstance = axios.create({
    baseURL: 'https://pc-shop-backend-git-doquang227-dev.apps.sandbox-m3.1530.p1.openshiftapps.com/',
    timeout: 100000,
    headers: {
        'Content-Type': 'application/json',
    },
});

axiosInstance.interceptors.request.use(
    (config: any) => {
        const userInfo: LoginResponse = JSON.parse(
            String(localStorage.getItem('authInfo'))
        );
        if (userInfo?.access_token) {
            config.headers['Authorization'] = `Bearer ${userInfo.access_token}`;
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axiosInstance.interceptors.response.use(
    response => {
        return response;
    },
    async error => {
        const { response } = error;
        if (response && response.status === 401) {
            localStorage.removeItem('authInfo');
            return;
        } else {
            // Handle other errors
            console.error('Error:', error.message);
            return Promise.reject(error);
        }
    }
);

export default axiosInstance;

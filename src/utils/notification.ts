import { notification } from 'antd';

type NotificationType = 'success' | 'info' | 'warning' | 'error';

export interface INotify {
    status: NotificationType;
    message: string;
    description: string;
}

export function showCustomNotification({
    status,
    description,
    message,
}: INotify) {
    if (status) {
        notification[status]({
            message: message,
            description: description,
        });
    }
}

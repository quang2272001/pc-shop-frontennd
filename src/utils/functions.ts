import { validate } from 'class-validator';
import { showCustomNotification } from './notification';
import { LoginResponse, Order } from '@/types/response.type';

export async function validateData(data: any) {
    const response = await validate(data);
    if (response.length <= 0) return true;
    for (const error of response) {
        showCustomNotification({
            status: 'error',
            message: 'Lỗi',
            description: `${error.property} không đúng định dạng!`,
        });
        return false;
    }
}

export function getUserInfo() {
    const userInfo: LoginResponse = JSON.parse(
        String(localStorage.getItem('authInfo'))
    );
    return userInfo;
}

export function converMoney(price: number) {
    return new Intl.NumberFormat('vi-VN', {
        style: 'currency',
        currency: 'VND',
    }).format(Number(price));
}

export function totalMoney(order: Order) {
    let sum = 0;
    for (const productOrder of order.listProduct) {
        sum += productOrder.product.price * productOrder.quantity;
    }
    return converMoney(sum);
}

export function handleException(error: any) {

    for (const message of error?.response?.data?.message) {
        showCustomNotification({
            status: 'error',
            message: 'Lỗi',
            description: message,
        })
    }
}
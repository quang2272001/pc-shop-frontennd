export const ERROR_MESSAGE: any = {
    username_or_email_existed: 'username_or_email_existed',
    username_or_password_incorect: 'email hoặc mật khẩu không đúng!',
    user_forbiden: 'user_forbiden',
};

export enum RECEIVE_TYPE {
    RECEIVE_AT_HOME = 0,
    RECEIVE_AT_STORE = 1,
}

export enum ROLE {
    ADMIN = 0,
    USER = 1,
}

export enum ORDER_STATUS {
    created = 'created',
    shipped = 'shipped',
    received = 'received',
    cancled = 'cancled',
    all = '',
}
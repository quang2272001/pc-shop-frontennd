import { Layout } from 'antd';
import Head from 'next/head';
import { useEffect, useRef, useState } from 'react';

import Header from '@/components/Header';
import { useRouter } from 'next/router';
import { LoginResponse } from '@/types/response.type';
import { getUserInfo } from '@/utils/functions';

const { Content } = Layout;

const DefaultLayout = ({ children }: any) => {
    const router = useRouter();
    useEffect(() => {
        const userInfo = getUserInfo();
        if (!userInfo) {
            if (
                router.pathname !== '/login' &&
                router.pathname !== '/register'
            ) {
                router.push('/login');
            }
        } else {
            if (router.pathname == '/login' || router.pathname == '/register') {
                router.push('/');
            }
        }
    }, []);
    return (
        <>
            <Head>
                <title>Pc Shop</title>
                {/* <link rel="icon" href="/icons/logovnpt.svg" /> */}
            </Head>
            <Layout className="min-content pt-0 bg-white-0 overflow-hidden">
                <Header />
                <Content className="bg-white-0 md:px-20 lg:px-52">
                    <Layout color="white" className="min-content">
                        <>
                            <Content className="bg-white-0 px-6 py-3">
                                {children}
                            </Content>
                        </>
                    </Layout>
                </Content>
            </Layout>
        </>
    );
};

export default DefaultLayout;
